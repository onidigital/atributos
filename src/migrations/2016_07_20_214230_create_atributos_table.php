<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAtributosTable extends Migration {

	public function up()
	{
		Schema::create('atributos', function(Blueprint $table) {
			$table->increments('id');
			$table->string('nome');
			$table->string('descricao');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('atributos');
	}
}