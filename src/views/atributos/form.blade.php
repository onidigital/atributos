@extends('layouts.admin')

@section('content')
@if(!isset($registro->id))
    {!! Form::open(['url' => $caminho, 'files'=>true ]) !!}
@else
    {!! Form::model($registro, ['url' => $caminho.$registro->id, 'method'=>'put', 'files'=>true]) !!}
@endif
    {!! Form::hidden('id', null) !!}
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="header">
               
            </div>
            <div class="content">
                    <div class="form-group" >
                        <div class="row">
                            <div class="col-sm-6" >
                                {!! Form::label('nome', 'Nome da Categoria:') !!}
                                {!! Form::text('nome', null, ['class' => 'form-control', 'autofocus required'] ) !!}
                            </div>

                        </div>
                    </div>

                    <div class="form-group" >
                        {!! Form::label('descricao', 'Descrição:') !!}
                     {!! Form::text('descricao', null, ['class' => 'form-control'] ) !!}
                    </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                @include('admin._partes._botao_voltar')
                {!! Form::submit('Gravar', ['class' => 'btn btn-fill btn-wd btn-success pull-right']) !!}
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}

@endsection