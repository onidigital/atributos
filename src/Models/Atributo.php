<?php

namespace Onicmspack\Atributos\Models;

use Illuminate\Database\Eloquent\Model;

class Atributo extends Model
{
    //
    protected $fillable = [
                           'nome',
                           'descricao',
                   
    ];

    // Para retornar o arquivo do slide:
    public function arquivo()
    {
        return $this->belongsTo('Onicmspack\Arquivos\Models\Arquivo', 'imagem');
    }
}