<?php
namespace Onicmspack\Atributos;

use Onicmspack\Atributos\Models\Atributo;
use Onicmspack\Arquivos\Models\Arquivo as Arquivo;
use Onicmspack\Atributos\Requests\AtributosRequest as AtributosRequest;
use Onicms\Http\Controllers\Controller;

class AtributosController extends Controller
{
    public $caminho = 'admin/atributos/';
    public $views   = 'admin/vendor/atributos/';
    public $titulo  = 'Atributos';

    public function index()
    {
        $registros = Atributo::all();
        $registros = configurar_status_toogle($registros, $this->caminho);
        return view($this->views.'.index',['registros'=>$registros],[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function create()
    {
        $html_toggle = gerar_status_toggle( array('status' => 1) );
        return view($this->views.'.form',[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
                    'html_toggle' => $html_toggle,
               ]);
    }

    public function store(AtributosRequest $request)
    {
        $input = $request->all();
        $input['imagem'] = $this->upload($input);
        Atributo::create($input);

        $request->session()->flash('alert-success', config('mensagens.registro_inserido'));
        return redirect($this->caminho.'create');
    }

    public function show($id)
    {
        $registro = Atributo::find($id);
        $html_toggle = gerar_status_toggle( $registro );
        return view($this->views.'.form', compact('registro'),[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
                    'html_toggle' => $html_toggle,
               ]);
    }

    public function update(AtributosRequest $request, $id)
    {
        $input = $request->all();

        if(isset($input['remover_arquivo'])){
            foreach($input['remover_arquivo'] as $arq)
                $input[$arq] = null;
        }

        // se mudou o arquivo:
        if(isset($input['imagem']))
            $input['imagem'] = $this->upload($input);

        $update = Atributo::find($id)->update($input);

        $request->session()->flash('alert-success', config('mensagens.registro_alterado'));
        return redirect($this->caminho.$id.'');
    }

    public function destroy($id)
    {
        Atributo::find($id)->delete();
        return redirect($this->caminho);
    }

    public function upload($input)
    {
        if(!empty($input['imagem'])){
            $manipulador = new Arquivo;
            $file = $manipulador->add($input['imagem']);
            // recortar?
            $manipulador->recortar('atributos', 'imagem', $file->id);
            return $file->id;
        }
        return false;
    }

    // Atualiza um campo boolean de um registro via ajax
    public function atualizar_status($id, $coluna = 'status')
    {
        // Verifica o status atual e dá um update com o novo status:
        $registro = Atributo::find($id);
        // Se encontrou o registro:
        if(isset($registro->{$coluna})){
            $novo = !$registro->{$coluna};
            $update = Atributo::find($id)->update( array( $coluna =>$novo ) );
            $resposta['success'] = 'success';
            $resposta['status']  = '200';
        }else{
            $resposta['success'] = 'fail';
            $resposta['status']  = '0';
        }
        return \Response::json($resposta);
    }

}
