<?php

Route::group(['middleware' => ['web','auth', 'authorization'], 'prefix' => 'admin' ], function () {
    // Menu do slide:
    Route::resource('atributos', 'Onicmspack\Atributos\AtributosController');
    Route::post('atributos/{id}/atualizar_status','Onicmspack\Atributos\AtributosController@atualizar_status'); // Atualizar Status Ajax
});